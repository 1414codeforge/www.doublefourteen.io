---
title: Team
menu_title: "The DoubleFourteen Code Forge Team"
mobile_menu_title: "1414° Team"
get_in_touch: true
date: 2022-03-10T00:40:51+02:00
---

{{< flexcolumn style="justify-content:center;align-items:center" >}}

{{% div class="container cards-list" style="width:75%" %}}

{{% div %}}
**Lorenzo Cogotti** — founder of The DoubleFourteen Code Forge, software architect,
game developer. He promotes free-software, open-source, practice-based research,
and common wisdom:
{{% div style="text-align:center" %}}
*"Any life worth living has enough time to play video games."*
{{% /div %}}
{{% /div %}}

{{% spacer %}}

{{% div %}}
**Katerina Zykova** — communication manager, master in innovation and puzzle fun.
Stand for equality, personal approach, and memes.
{{% /div %}}

{{% spacer %}}

{{% div %}}
**Andrea Pasquini** — a game developer skilled in various languages from JavaScript to C++.
He likes developing software for fun. Entering the video game development world
inspired by retro games and his beloved Monkey Island I and II.

Plus, Andrea's in charge of sports in our team: surfing, hiking? He can do it all. 
{{% /div %}}

{{% spacer %}}

{{% div %}}
**Sara Pintus** — our fantastic concept artist with experience in video games and digital transformation.
She firmly believes that art is "brain food" capable of making the world a better place.

To her, happiness is pizza and a good game.
{{% /div %}}

{{% spacer %}}

{{% div %}}
**Crescendo** — composer, audio designer and average bard, all included: making music, granting buffs, and cracking bad jokes, possibly all at once.
Card games enthusiast and robots enjoyer, will play anything with a good story and lots of talking... and if no one's talking, they'd better be singing. 
{{% /div %}}

{{% /div %}}

{{< /flexcolumn >}}
