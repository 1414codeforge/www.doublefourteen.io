---
title: "Portfolio"
menu_title: "The DoubleFourteen Code Forge Portfolio"
mobile_menu_title: "1414° Portfolio"
get_in_touch: true
date: 2022-03-10T00:40:51+02:00
---

## Software

**Kronos Production Analyser**

Kronos is an automatic production process analysis system based on publications
and works by Professor Mario Morroni
([bio](https://it.wikipedia.org/wiki/Mario_Morroni) in Italian)
and Professor Vittorio Moriggia
([bio](https://didattica-rubrica.unibg.it/ugov/person/3026) in Italian).
It splits a single production process into multiple phases, summarizing
each phase's monetary and time expenses, underlying costs, revenues and
workforce involved.

*It helps optimize business processes.*

**Micro BGP Suite**

Micro BGP Suite is a suite of libraries and utilities.
It enables efficient processing of BGP data encoded in the most widely available
formats, and allows interesting network traffic analysis at the routing level.

*It makes the Internet better.*

## Video games
{{< flexrow >}}

{{% div style="margin-right:30px" %}}
Our great mystery.

*It is fun to make.*
{{% /div %}}
{{% div %}}
![Hack in progress...](wip.png)
{{% /div %}}

{{< /flexrow >}}

## Consultancy and education courses

The DoubleFourteen Code Forge promotes knowledge sharing.
So, we offer one-on-one sessions and education courses on software development,
digitalization, and online marketing.

*It helps our minds.*

## Code review

The DoubleFourteen Code Forge aims to improve software.
So, we offer a code-review service to advise on the best code practices
to build scalable, reliable and maintainable software.

*It helps everyone.*
