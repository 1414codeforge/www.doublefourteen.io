---
title: Home
get_in_touch: true
tags:
  - development
  - coding
  - hacking
  - free software
  - open source
  - high performance
  - research
  - computer science
---

{{< flexcolumn style="justify-content:center;align-items:center" >}}
{{% div class="headline" %}}
**The DoubleFourteen Code Forge makes digital better.**

{{% div style="text-align:left" %}}
We believe in the synergy between conscious business, research and coding.
{{% /div %}}
{{% /div %}}
{{% div style="align-self:flex-center" %}}
<a href="doublefourteen/" class="button">See our manifesto</a>
{{% /div %}}
{{< /flexcolumn >}}

{{% div class="alt-background" %}}
{{% div class="container cards-list" %}}

{{< flexcolumn >}}

{{% div title="Get our help" %}}
{{% div style="text-align:left" %}}
- See how <a class="prose-link" href="portfolio/">digital tools</a> can help you.
- Get <a class="prose-link" href="contacts/">advice</a> on your digitalization.
- Create your new software <a class="prose-link" href="team/">with us</a>.
{{% /div %}}
{{% /div %}}

{{% div title="Join us" %}}
{{% div style="text-align:left" %}}
- <a class="prose-link" href="blog/">Official blog</a> to share our thoughts.
- <a class="external-prose-link" href="https://git.doublefourteen.io/explore/repos">Git repository</a> to share our code.
- IRC on `#doublefourteen`@<a class="external-prose-link" href="https://libera.chat">Libera.Chat</a>.
{{% /div %}}
{{% /div %}}

{{< /flexcolumn >}}

{{% /div %}}
{{% /div %}}
