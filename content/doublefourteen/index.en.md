---
title: DoubleFourteen
menu_title: "The DoubleFourteen Code Forge"
mobile_menu_title: "1414° Code Forge"
date: 2021-06-15T00:40:51+02:00
---

**The DoubleFourteen Code Forge** creates meaningful games and software to promote synergy between developers, researchers, and businesses.

We develop safe, efficient, high-quality solutions using the latest research and our fantastic community.

# Raison d'être

**Community**

The DoubleFourteen Code Forge constructs the ecosystem where researchers, developers, and enthusiasts work together. We strive to create value beyond the digital world as we develop more accessible technologies and ensure the transparency of our projects through direct communication. So, DoubleFourteen aims to improve everyday life, carry social value and raise awareness of computer science and new technologies.


**Coding**

We consider code to be an art form, and we put its beauty to a good cause. DoubleFourteen promotes ethical software that respects users’ or developers’ rights and privacy.

**Knowledge**

The DoubleFourteen Code Forge believes computer science should be a harmony between theory and practice. But unfortunately, the theoretical part is often underlooked by practitioners, while practical usage is a minor detail for researchers. In contrast, we think knowledge exchange is the key to innovation and creativity. DoubleFourteen is the union of research, practice, and fun.

We always welcome new members, projects, and research ideas!

# Fundamental principles

* We never compromise over quality. Our work is our art and pride.

* We are eager to learn, grateful to teach, and inspired to work.

* We value research, code, experience, knowledge and share them publicly.

* We promote ethical software. The product/service we create/endorse/recommend must **never**:

    - mistreat users or developers;
    - violate the users' or developers' privacy or silently collect their data;
    - severe the right to understand, learn, verify, modify or improve the original source code;
    - deny full control over the computing instruments;
    - tie the user or developer to a single software or service, constraining their freedom of choice.

* We regard our independence as a vital premise.

# Community rules

* We believe in civilized debate. Personal offenses and discrimination in any public or private form are incompatible with our community.

* We encourage self-expression, experiments, and continuous learning that are not bound by censorship, industry interest, business secrets, etc.

* We keep any social, political, religious, or ideological conviction to ourselves. Our community as a whole is neutral and refuses to take any stance or action whatsoever over any matters but the scientific debate on software development, computer science, and video games.

* We encourage any benevolent participation.
