---
title: "NotGDC 2023"
mobile_menu_title: "NotGDC 2023"
get_in_touch: true
date: 2023-03-23
news_keywords: [ "NotGDC", "LOVE2D", "Game Development", "gamedev", "Yui", "user interface", "UI" ]
---

## NotGDC 2023

During [NotGDC 2023](https://notgdc.io) we introduce [Yui](https://gitea.it/1414codeforge/yui) -- **Yet another User Interface** library.


You can use it to effortlessly create user interfaces for your game,
using the [LÖVE](https://love2d.org) 2D game engine.
Its main features are:

- Declarative approach to interface building
- Extensible widget library
- Out of the box localization support
- Intuitive widget layout on screen
- Built-in navigation via mouse, joystick or keyboard
- Reasonably small and hackable codebase

The following slides summarize **Yui**'s strong points and give a taste of how it looks and feels - on code and on screen.

Enjoy!

[Download PDF](notgdc-2023-yui.pdf)
