---
title: Contacts
menu_title: "The DoubleFourteen Contacts"
mobile_menu_title: "1414° Contacts"
get_in_touch: true
date: 2022-03-10T11:40:51+02:00
---

The DoubleFourteen Code Forge is always looking for motivated people,
projects and ideas!

# Discuss your project

- Contact us to get your digital solution: {{< mail >}} info@doublefourteen.io {{< /mail >}}

# Get involved

The DoubleFourteen Code Forge initiative promotes digitalization and independent research.

- Join `#doublefourteen` on IRC at <a class="external-prose-link" href="https://libera.chat">Libera.Chat</a>.
- Check Code Forge's updates, news, and memes on Twitter <a class="external-prose-link" href="https://twitter.com/1414codeforge">@1414codeforge</a>.
- Discuss our code on the official <a class="external-prose-link" href="https://git.doublefourteen.io/explore/repos">Git Repository</a>.
- Fork us on <a class="external-prose-link" href="https://github.com/doublefourteen">GitHub</a>.

-----

We would love to know if our code is being useful to you ♥
