---
title: "Old school IRC. Kat-arena"
mobile_menu_title: "IRC. Kat-arena"
date: 2022-04-04
Author: "Katerina"
description: "Kat-arena goes on exploring IRC after the introduction of the brave new DoubleFourteen Code Forge channel."
series: [ "Kat-arena" ]
categories: [ "communication", "marketing" ]
tags: [ "Kat-arena", "business", "marketing", "digitalization", "irc", "Internet Relay Chat"]
---

What is IRC and why using it today?

If you have not been around (the Internet) in the early 00s, chances are you have
never heard about Internet Relay Chat.

However, the DoubleFourteen Code Forge decided to open one in 2022,
and you might want to join us on `#doublefourteen` at [Libera.Chat](https://libera.chat).

IRC was invented in 1988 by Jarkko Oikarinen, who aimed to give users outside the
network a chance to participate in conversation without creating a profile.
So, it allows users to provide as much or little info about themselves as they are
comfortable with, starting from zero.

Nowadays, it sounds almost radical, considering you have to accept GPS tracking
to use some services or enter your birthday to check the news.
As a matter of fact, it was not. IRC was born in the early days of the Internet,
when the respect for users' privacy was yet to be taken over by user's data
monetary value.

So, Internet Relay Chat is a system (although, being decentralized, it may be described as the absence of one)
of networks, channels, and chat rooms.
Originally text-based, the same technology now supports different mediums
including video, although its traditionally minimalistic interfaces are
still more text-friendly.

Why choose IRC today?

Because the same points that ensured its success in the 90s are still relevant today:

*For business* it is an easy and stable tool that can be added directly to a website
and ensure that customers can reach you directly in one click even if they are
off social media or Instagram/Facebook/Google services are down.

And *for everyone*:

1. *extremely lightweight*, useful even with limited bandwidth or jittery connections.
2. *flexible*. it offers both group chats and one-on-one conversations.
3. *simple* the interface is simplicity as it is.
4. *decentralized* anyone, in principle, can set up an IRC network*.
5. *free* like, actually free, not "pay later", not "here are some ads",
   and definitely not "we collect and sell your data to third parties".

*Note*: in principle, yes, but most certainly you will use one of the existing networks.
While transitioning between them is fairly simple, I want to point out that the security and privacy
of IRC's networks are mostly based on transparency and its community
(same as with many Open Source initiatives).
Hence, I want to encourage you to stay attentive and avoid focusing on the same
service or platform. IRC gives you freedom, so use it wisely.

Last thought: while exploring IRC, I have realized that numerous easy messengers
I know are inspired by these "old school" channels.
So, apart from all the technical bonuses and safety, it is also extremely
intuitive and requires zero to little adaptation.

So, maybe along with vinyl, Polaroids, and black-and-white movies we can bring back IRC?

Katerina
