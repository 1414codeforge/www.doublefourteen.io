---
title: "Treat your email right. Kat-arena"
mobile_menu_title: "Treat your email right"
date: 2022-06-30
Author: "Katerina"
description: "Kat-arena is trying to make sure her major internet identification, the email, is secured nicely. but getting rid of spam for free is not that easy."
series: [ "Kat-arena" ]
categories: [ "software", "privacy" ]
tags: [ "email", "privacy", "tracking", "Free Open Source Software", "FOSS" ]
news_keywords: [ "email", "privacy", "tracking", "Free Open Source Software", "FOSS", "webmail", "webmail system" ]
---

# Treat your email right

Email was born as a more convenient and rapid way to exchange messages
in an attempt to replace physical letters.
Indeed, this is what has happened in the 50 years that passed since its inception.
Nowadays email is considered an absolute basic communication medium.
It is required for accessing almost any digital, and sometimes physical, service
(even the dentist asked for my email address!).
As such, an email address may convey more personal details than a physical one.

So, today seems like a good time to update your email safety.

Your email address is split in 3 major parts
(as specified by [RFC-5322](https://datatracker.ietf.org/doc/html/rfc5322#section-3.4.1)):

```goat { width=400 height=240 }
 .-------------------------. .-----------.
 |username (the local-part)| |Domain name|
 '---+---------------------' '-+---------'
      \                       /
       \                     /
        username @ domain.com
                 |
                 |
        .--------+----------.
        |at-sign separates  |
        |username and domain|
        '-------------------'
```

Usually the domain name conveys information about your affiliation or your email
service provider.

Having multiple emails, unaffiliated with a company,
is a common way to protect your digital privacy, improve decentralization
of the Internet, and help confining and reducing spam.

To put it in Lorenzo Cogotti's words:

> The strategy works by making it harder to associate different services'
> subscriptions together by a single email address, reducing the chance to
> track you. Keep in mind though, that other information may still be used
> to link them together (e.g. there's still your phone number).

## Some basic tips

Let's cover some basics on how you can improve your safety even with
non Free and Open Source (FOSS) email solutions.

* To minimize tracking, don't open your inbox inside a browser
  (**webmail** **client**), prefer installing specific software
  (**mail client**, **email reader**).
  Otherwise, the safety of the inbox contents is in the hand of the browser
  as much as the email provider.
* Avoid sending "heavy" files or any important document via email.
  The first ones have a high chance of never reaching the recipient due to
  attachment filters. While the other ones may be scanned automatically by your
  email service provider or accessed in case your email password's leaked.
  Some alternatives are, for example, file-sharing platforms with restricted
  access or actual physical devices.
* Use different accounts for social media authorization,
  e-commerce and subscription services. Do not share this email anywhere.
  The same goes for deliveries.
* For any suspicious website or one-time promo solutions,
  consider an auto-generated email with an automatic verification function
  (below).

This won't be enough to ensure you won't receive spam.
In practice, we have to accept nothing on the Internet can guarantee you a 100%
spam-free life, unless you use services expressly designed to fight spam and
promo emails. However, it will make the lives of those who are trying to
reach you, for this reason, a bit more complicated.

## What about email service providers?

This partially settles the problem with other services using email addresses to track us,
but what if we suspect **the email service provider** is the one scanning us?

Probably, the best bet would be configuring your own mail server.
Unfortunately, it cannot be considered gratis or easy, but if you are a
business owner or an enthusiast it may be viable.
Creating a domain is not enough, as managing emails needs storage space
on a server. Which practically means buying a piece of hardware.
Prices range from 30 to several thousands euros, with the basic options being
a good solution for small initiatives and individuals.
For some business owners, the hardware price and the increase in energy bills
may justify renting a server.
Though, running it still requires advanced knowledge and maintenance.

The average user may look for simpler solutions, like relying on existing
servers provided by dependable mail service providers.
Surely, it is not that easy.

Let's introduce a sad fact about our world.

> Nothing is gratis.

As we have just seen, running a mail hosting server is not trivial.
It involves expensive hardware, complex knowledge, storage space,
disaster recovery plans, web traffic distribution and Internet domain management,
to name a few, - so, it is naïve to expect it all free of charge.

In fact, it is not.
Even when there is no subscription fee, one should look for a more complex one.
Most "gratis" mail services actively track their users' data,
for further monetization, such as targeted advertising.
In other words, some mail servers may scan your emails or share the history of
your message with third parties and governments.
This practice is not considered illegal, as some of this data is
not regarded as personal or sensitive.
Plus, a user should be aware of everything as they are always welcome to read
the 20+ pages of Privacy Notice to understand how their data is treated
(and do not forget to check yearly privacy updates timely).

Additionally, big famous mail servers are the most popular targets of attacks
and consequent password leaks, plus their email addresses are easier to guess.
As for smaller ones, if you are unsure where to look, it is not obvious to
avoid third-party involvement and ensure data safety.
For example, numerous services still use ReCaptcha, a service by Google
(the existential "I am not a robot" thing).

So, it is important to stay informed and make sure you understand how the
service treats your data and who can access it.

The [Free Software Foundation](https://www.fsf.org/resources/webmail-systems) provides some interesting insights on the subject.

Stay informed, and stay safe!

Katerina
