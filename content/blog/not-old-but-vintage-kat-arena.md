---
title: "Not old but vintage"
mobile_menu_title: "Not old but vintage"
date: 2022-05-11
Author: "Katerina"
description: "Kat-arena discovers software aging, a topic ever more relevant today with apps update limits and Preservation Teams for every kind of soft from video games to OSes. Just one question: how a piece of code can age?"
series: [ "Kat-arena" ]
categories: [ "software", "open source" ]
tags: [ "Kat-arena", "software aging", "open source", "software obsolescence" , "feature creep", "bloatware", "software bloat", "software rejuvenation", "planned obsolescence"]
news_keywords: [ "software aging", "open source", "software obsolescence" , "feature creep", "bloatware", "software bloat", "software rejuvenation", "planned obsolescence" ]
---

# Not old, but vintage

Recently, Sony announced the creation of a Game Preservation Team,
and the App Store seems to be removing apps that haven’t been
"updated in a significant amount of time". Nowadays, digital preservation
initiatives focus both on hardware and software components,
but it seems tricky. Surely, "Time destroys all things".
However, the software part is not exactly a *thing*, it is a set of
mathematical formulas. So, does it get old?

The short answer: yes, it does.

> "Just as we will all (if we are lucky) get old, software aging can, and will occur in all successful products."
>
> -- David Lorge Parnas, Software aging (1994)

However, you are not here for short answers, aren't you?

## The many ways to age

Software ages, and does so in various ways.
Mostly, since software is always part of a complex environment that
sometimes includes other software or even humans.
Naturally, each part is affected by changes on other levels.
Whether they are planned or unintentional, desired or problematic - any change can lead to the software aging.
In fact, it is mostly a question of the aging environment surrounding the soft.
Still, the chain of interactions in time might become so perplexing that
separating software's local issues from external factors might become impossible.

With this, scientists define *aging* as the accumulation of errors over time in long-running software.
While bugs in the code are the main bad guys, the list of antagonists includes data corruption,
wrong commands, faulty statements, and unexpected interactions
with other software - you know, life is a dangerous endeavor.

On a basic user level, these issues are often solved by rebooting the entire system and cleaning the memory.
Yet, it is solving the issue in the same way as the "burning down the house to get rid of a scary insect" approach.

Clearly, the best way to prevent this sort of aging and ensure the system's reliability
is to get rid of bugs in every single software. However, pretty much like in real life - all bugs can never be found...
plus, some of them are features, right?

I promise I will dive deeper into bugs someday, meanwhile, you may want to check Garg's study (reference below)
to dig further into this subject.

As the subject is extremely compound, an entire line of research dedicated to solving the issue of software
aging is yet to find a perfect solution. If you are looking for some good suggestions, see the software rejuvenation.

However, aging may have a different interpretation that is way closer to actual users.
It is the trend for a certain technology.

For developers, it implies a constant necessity to update their products.
Surely, some of them are vital and greatly improve efficiency, correct bugs, or affect core functions,
but other "new versions" and "cool features" have little to do with real issues.

In other words, there are numerous perfectly functioning "old" technologies that can be used today and
sometimes are even easier to run and introduce, but they are just "not cool" anymore.

For example, how many companies have introduced Augmented Reality features to their products
following the Pokémon Go boom? And how many users have activated it in the last year?
The poor result of a constant rush for new features is referred to as **Feature creep.**

At the same time, new features make code bigger, thus creating more space for bugs.
Those new bugs, in turn, increase the aging process and the demand for new updates.
Curiously, this phenomenon has an amazingly accurate name - **Software bloat,** or **Bloatware.**

Altogether, it seems like an eternal dev circle where instead of improving an existing product,
companies keep adding new features to stay cool. Fair to say, for many developers and companies,
it becomes so unbearable it is easier to abandon a project completely.

Compatibility is another issue strongly connected with unbearable development. Software can be
labeled as *old* because it was agreed to be claimed so. This type of old may have
little to do with actual functionality or is often planned by *fundamental* soft
producers such as OS developers.

It is arguable whether compatibility requirements are completely evil.
The *pro* argument is that *planned aging* encourages software rejuvenation,
so it is a direct way to fight degradation. So, these updates allow a soft to be
"Harder, Better, Faster, Stronger".
However, this improvement is often caused by the bare fact that the *system* is new
which brings back the "burning the house down" approach.

Along with all the good it brings, it can also kill it completely,
or send the idea to development hell as it will be internally stuck in production unable
to catch the required schedule (think of Duke Nukem Forever or Diablo III).

Video gamers usually learn about compatibility thanks to
"the game is not supported by the X.Y version or later" or
"this title will not be transported to the next-gen consoles".
Both messages can be translated as "this developer sees no point in updating
it to match new requirements anymore".

## A way to live again

This painful question of whether devs should support their software by the end of time (rightfully)
causes a revolution in fandoms as fans go berserk.
Sometimes this rage brings changes by showing companies that the soft is more popular than it appears to be,
or by introducing an open-source practice.

Opening a codebase to other developers and users allows the community to directly participate in the development
and provide essential updates to a soft when its original creators are unable to do so, for whatever reason.
With this, open-sourcing is a powerful tool to fight both fast-changing fashions and planned obsolescence.
The product will continue to live (or even grow) as long as there are people interested in it.
Moreover, it can be rediscovered later - just like Bach's music, Kafka's literary heritage,
and many other great works and ideas.

That's why software supported by the community can receive a particular vintage or even antique charm.

Yes, it can be argued whether the initial concept behind a soft matches the result of community development,
but isn't it the most amazing thing about ideas - it is stories and how they are changing over time.

So, on a very poetic level, open sources allow software to fight the brutal degradation of living thing
and join the quasi-eternal existence of great ideas.
Quite an impressive answer to an initially lunatic question, I must say.

Katerina

## Sources

* **S. Garg, A. van Moorsel, K. Vaidyanathan and K. S. Trivedi, *"A methodology for detection and estimation of software aging",* Proceedings Ninth International Symposium on Software Reliability Engineering (Cat. No.98TB100257), 1998, pp. 283-292, DOI: 10.1109/ISSRE.1998.730892.**

* **David Parnas *"Software aging",* Invited Plenary Talk, Department of Electrical and Computer Engineering McMaster University, Hamilton, Ontario, Canada. 1994.**

* **Kishor Trivedi *"Software Aging and Software Rejuvenation"*, a lecture for the Networking and Information Technology on 11/05/2022 (accessed on 04/05/2022, <https://www.youtube.com/watch?v=4tO6p1fYeu8>)**

* **Michael Grottke, Rivalino Matias Jr, Kishor S. Trivedi *"The Fundamentals of Software Aging",* workshop at 19st International Symposium on Software Reliability Engineering, 2008 (accessed on 05/05/2022, URL <https://grottke.de/documents/FundamentalsOfSWAging.pdf>)**
