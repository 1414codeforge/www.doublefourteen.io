---
title: "The Micro BGP Suite has been released!"
mobile_menu_title: "µbgpsuite now released!"
date: 2021-06-15
description: "The DoubleFourteen Code Forge is happy to announce that ubgpsuite - The Micro BGP Suite - has been released: bgpgrep and lonetix are now available!"
series: [ "ubgpsuite - The Micro BGP Suite" ]
categories: [ "news", "development" ]
tags: [ "ubgpsuite", "bgpgrep", "lonetix", "bgpscanner", "C Language", "Networking", "BGP" ]
news_keywords: [ "ubgpsuite", "bgpgrep", "lonetix", "bgpscanner" ]
---

## The Micro BGP Suite is now available

I am thrilled to announce that the very first version of the
DoubleFourteen inaugural project, the Micro BGP suite (*µbgpsuite* for short
-- or *ubgpsuite* for differently Greek keyboards ☺), is now available!

Source code for this project is available at:
[https://git.doublefourteen.io/bgp/ubgpsuite](https://git.doublefourteen.io/bgp/ubgpsuite)

The Micro BGP Suite is an evolution over *bgpscanner*, originally developed under the
Institute of Informatics and Telematics of the Italian National Research Council,
see the forever unknown
[HISTORY](https://git.doublefourteen.io/bgp/ubgpsuite/src/branch/master/doc/HISTORY.md)
file included with the project documentation for more obscurities about this
project.

The Micro BGP suite includes:
- [lonetix](https://git.doublefourteen.io/bgp/ubgpsuite/src/branch/master/lonetix),
  a performance oriented static library for BGP and MRT data encoding/decoding written in C.
  I'd like to send a special thanks to my friend [Vernal Liu](https://covernal.github.io)
  for coming up with a name for this library (though, it originally meant
  *Lorenzo's Network library on Posix*), and for his saint-like patience in listening to
  my ramblings all the time.
- [bgpgrep](https://git.doublefourteen.io/bgp/ubgpsuite/src/branch/master/tools/bgpgrep),
  the very first utility using *lonetix*, an advanced replacement for *bgpscanner*,
  an utility capable of lightning fast MRT dump decoding and filtering. This utility
  is documented in its glorious `man` [page](https://git.doublefourteen.io/bgp/ubgpsuite/src/branch/master/tools/bgpgrep/bgpgrep.1.in).

`bgpgrep` is the first utility taking advantage of `lonetix`, but more tools
are underway to demonstrate the capabilities of its API.

> **Hint** -- for a convenient PDF version of the manual page, you can run:
> ```sh
> $ sed s/@UTILITY@/bgpgrep/g tools/bgpgrep/bgpgrep.1.in | groffer
> ```

Stay tuned for more news on this project, as I'm currently working on an article
describing the BGP filtering engine exposed by `lonetix`.

Enjoy, and happy hacking,

Lorenzo Cogotti
