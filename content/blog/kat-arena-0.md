---
title: "Hello, World! Kat-arena"
mobile_menu_title: "Hello, World! Kat-arena"
date: 2022-03-15
Author: "Katerina"
description: "Our communication manager answers the most common questions on digitalization and business developemt"
series: [ "Kat-arena" ]
categories: [ "communication", "marketing" ]
tags: [ "Kat-arena", "business", "marketing", "digitalization"]
---

Hey, this is Katerina

I am the communication manager behind  The DoubleFourteen Code Forge. Here, I answer common questions on digitalization and business and share my journey in the strange world of new technologies.

I don’t have a computer science background, and If you are looking for cool computer stuff - check our blog page. Here, my goal is to talk about real digital tools and video games from a general audience point of view, asking silly questions and sometimes translating my colleagues’ binary language to the human one.

Take care and play video games,

Katerina
