---
title: "New Micro BGP Suite utility peerindex now released!"
mobile_menu_title: "New utility peerindex is out!"
date: 2021-07-27
description: "The DoubleFourteen Code Forge is happy to announce that ubgpsuite has a new utility: peerindex!"
series: [ "ubgpsuite - The Micro BGP Suite" ]
categories: [ "news", "development" ]
tags: [ "ubgpsuite", "peerindex", "bgpscanner", "C Language", "Networking", "BGP" ]
news_keywords: [ "ubgpsuite", "peerindex", "bgpscanner" ]
---

## The Micro BGP Suite has a new utility: *peerindex*

I am happy to announce that *µbgpsuite* has a new tool in its arsenal, *peerindex*.

[peerindex](https://git.doublefourteen.io/bgp/ubgpsuite/src/branch/master/tools/peerindex)
is a simple PEER INDEX TABLE inspection tool documented in its own gorgeous `man` [page](https://git.doublefourteen.io/bgp/ubgpsuite/src/branch/master/tools/peerindex/peerindex.1.in).

When implementing *bgpgrep*, I removed any functionality not strictly related to BGP filtering dump,
this included the PEER INDEX TABLE inspection feature previously provided by *bgpscanner*, that is:
```sh
$ bgpscanner -f
```

This command used to display every PEER INDEX TABLE entry referenced by at least one RIB entry.
It was a useful tool to quickly recognize which peers populated a RIB dump.

*µbgpsuite* will get a dedicated tool for this task, the main topic of this article: *peerindex*.

In addition to showing only the referenced peers inside PEER INDEX TABLE, it can
also dump the full table as is.

> **Hint** -- as always, you can get a convenient PDF version of the manual page, like this:
> ```sh
> $ sed s/@UTILITY@/peerindex/g tools/peerindex/peerindex.1.in | groffer
> ```
>
> Don't you love man pages? ☺

Enjoy, and happy hacking,

Lorenzo Cogotti

