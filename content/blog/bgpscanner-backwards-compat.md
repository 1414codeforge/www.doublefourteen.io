---
title: "New bgpscanner backwards compatibility script released!"
mobile_menu_title: "bgpscanner compatibility script is out!"
date: 2021-08-11
description: "Meet the bgpscanner legacy wrapper - The Micro BGP Suite now provides mostly complete backwards compatibility with Isolario bgpscanner. You may use it to learn bgpgrep & peerindex or to run your legacy scripts easily."
series: [ "ubgpsuite - The Micro BGP Suite" ]
categories: [ "news", "development" ]
tags: [ "ubgpsuite", "bgpscanner", "bgpgrep", "peerindex", "Networking", "BGP" ]
news_keywords: [ "ubgpsuite", "bgpscanner", "bgpgrep", "peerindex" ]
---

## New bgpscanner backwards compatibility script is now available.

Lately I had the pleasure to be in contact with many old time `bgpscanner` aficionados.

A question popped up frequently during our conversations.
* I used to do `X` with `bgpscanner`, can `bgpgrep` do the same, and how?

That's when the fact hit me, that `bgpscanner` probably had a larger userbase relying
on it for day to day work than I originally anticipated.
Unfortunately `bgpgrep` and `peerindex` are not exactly backwards compatible.
While probably being very convenient to new users, it could throw off
the experienced userbase that has been familiar with `bgpscanner` for years.

Even worse, it could make existing scripts instantly unusable.

This convinced me that providing a backwards compatibility feature might be a good
idea to help users familiarize with the new tool while also providing a smoother
transition curve.

Coming back to the recurring question, yes, `bgpgrep`, and `peerindex`, do
anything `bgpscanner` can do.

To top it off, `bgpgrep` does what `bgpscanner` does not.

That was actually the most fundamental reason that encouraged me to break
backwards compatibility.
`bgpgrep` provides extreme flexibility in filtering specification, letting
the user combine freely multiple conditions and chain them arbitrarily with `AND`,
`OR` and `NOT` logic.
Something `bgpscanner` could do only to a very limited extent.
This while also being intuitive with its syntax.

But that doesn't eliminate the fact that `bgpscanner` was already pretty powerful
by itself, and it did things in a different way. So what?

Enter the [`bgpscanner` legacy wrapper script](https://git.doublefourteen.io/bgp/ubgpsuite/src/branch/master/tools/bgpscanner).
The third tool in µbgpsuite arsenal (they grow so fast ♥).

A somewhat pragmatic, crude, grumpy -- yet fascinating in its own special way,
POSIX shell wrapper script capable of converting (almost) any invocation of
`bgpscanner` to an equivalent invocation of `bgpgrep` or `peerindex`.
Making the `bgpscanner` legacy wrapper a drop-in replacement for the
classic Isolario `bgpscanner`.

While this is satisfactory by itself, it didn't feel quite alright.
In my book `bgpscanner` is still an obsolete tool (hence the "legacy" in "wrapper script").
Being able to actually *learn* how to do things with new tools would be
much better than relying on the old ways.

This is when it hit me again, the wrapper script already does all the hard work
to figure out how to make an equivalent call to µbgpsuite native tools.
Why not just print the result, instead of executing it?

```sh
$ PRETEND=y bgpscanner -i 192.65.131.1 -p "137 2598$" -u 8.8.8.8/32 data/rib/bview.20181016.0800.gz
bgpgrep -- data/rib/bview.20181016.0800.gz -peer \( 192.65.131.1 \) -supernet \( 8.8.8.8/32 \) -aspath "137 2598$"
```

Isn't it awesome? With just a tiny `PRETEND=y` flag, we can now interactively learn
how to convert an old invocation to the new format.
True, it might not be the best, most expressive, most idiomatic way to write the query,
but it is an excellent starting point.

You can install the `bgpscanner` legacy wrapper script along with other µbgpsuite
tools by enabling the `bgpscanner-legacy` configuration option with `meson`,
just run:
```sh
$ meson configure -Dbgpscanner-legacy=true
```
inside your µbgpsuite build directory and it will be installed along with the
other tools upon `ninja install`.

Obviously, it wouldn't be a true announcement post, if I didn't also mention
that the `bgpscanner` legacy wrapper script is documented in its own fantastic
man [page](https://git.doublefourteen.io/bgp/ubgpsuite/src/branch/master/tools/bgpscanner/bgpscanner.1.in).
And that you're just one command away from having a beautiful PDF with its contents:
```sh
$ sed s/@UTILITY@/bgpscanner/g tools/bgpscanner/bgpscanner.1.in | groffer
```

Hoping the `bgpscanner` legacy wrapper tool will be at least as useful to you as
it was fun for me to write.

Happy hacking!

Lorenzo Cogotti

> P.S. A gigantic and special thank you to [Rich's sh (POSIX shell) tricks](https://www.etalabs.net/sh_tricks.html),
> for its enlightening list of shell recipes. In my opinion one of the most fundamental reads that anybody writing
> a shell script should go through.
