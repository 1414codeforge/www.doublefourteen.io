#!/bin/sh

readonly PNGCRUSHOPTS="-reduce -brute -ow"

# Favicon
#########

for i in 152 144 120 114 180; do
  rsvg-convert -f png -w ${i} -h ${i} "static/favicon.svg" > "static/favicon-${i}-precomposed.png"
  pngcrush ${PNGCRUSHOPTS} "static/favicon-${i}-precomposed.png"
done
for i in 57 32; do
  rsvg-convert -f png -w ${i} -h ${i} "static/favicon.svg" > "static/favicon-${i}.png"
  pngcrush ${PNGCRUSHOPTS} "static/favicon-${i}.png"
done

convert "static/favicon.svg" -bordercolor white -border 0 \
  \( -clone 0 -resize 16x16 \) \
  \( -clone 0 -resize 32x32 \) \
  \( -clone 0 -resize 48x48 \) \
  \( -clone 0 -resize 57x57 \) \
  \( -clone 0 -resize 64x64 \) \
  \( -clone 0 -resize 72x72 \) \
  \( -clone 0 -resize 110x110 \) \
  \( -clone 0 -resize 114x114 \) \
  \( -clone 0 -resize 120x120 \) \
  \( -clone 0 -resize 128x128 \) \
  \( -clone 0 -resize 144x144 \) \
  \( -clone 0 -resize 152x152 \) \
  -delete 0 -alpha off -colors 256 "static/favicon.ico"

# Other scalable logos/images
#############################

for i in 16 32 48 57 64 72 110 114 120 128; do
    NAMTWITBLUE="twitter-logo-blue-${i}.png"
    NAMTWITWHIT="twitter-logo-white-${i}.png"
    NAMMAILGRAY="mail-gray-${i}.png"
    NAMMAILWHIT="mail-white-${i}.png"

    convert "static/twitter-logo-blue.png" -resize ${i}x${i} "static/${NAMTWITBLUE}"
    pngcrush ${PNGCRUSHOPTS} "static/${NAMTWITBLUE}"
    convert "static/twitter-logo-white.png" -resize ${i}x${i} "static/${NAMTWITWHIT}"
    pngcrush ${PNGCRUSHOPTS} "static/${NAMTWITWHIT}"
    convert "static/mail-gray.png" -resize ${i}x${i} "static/${NAMMAILGRAY}"
    pngcrush ${PNGCRUSHOPTS} "static/${NAMMAILGRAY}"
    convert "static/mail-white.png" -resize ${i}x${i} "static/${NAMMAILWHIT}"
    pngcrush ${PNGCRUSHOPTS} "static/${NAMMAILWHIT}"
done

