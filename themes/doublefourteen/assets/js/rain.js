/* -----------------------------------------------
 * Rain background particle effect.
 *
 * @licstart  The following is the entire license notice for the
 *            JavaScript code in this page.
 *
 *   Copyright (C) 2020  Lorenzo Cogotti
 *
 *   The JavaScript code in this page is free software: you can
 *   redistribute it and/or modify it under the terms of the GNU
 *   General Public License (GNU GPL) as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option)
 *   any later version.  The code is distributed WITHOUT ANY WARRANTY;
 *   without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 *   As additional permission under GNU GPL version 3 section 7, you
 *   may distribute non-source (e.g., minimized or compacted) forms of
 *   that code without the copy of the GNU GPL normally required by
 *   section 4, provided you include this license notice and a URL
 *   through which recipients can access the Corresponding Source.
 *
 * @licend  The above is the entire license notice
 *          for the JavaScript code in this page.
 * ----------------------------------------------- */

particlesJS("particles-bg",

  {
    "particles": {
      "number": {
        "value": 80,
        "density": {
          "enable": true,
          "value_area": 200
        }
      },
      "color": {
        "value": window.getComputedStyle(
                     document.getElementById("particles-bg")
                 ).getPropertyValue("--notfound-particles-color").trim()
      },
      "shape": {
        "type": "circle"
      },
      "opacity": {
        "value": 0.3,
        "random": true,
        "anim": {
          "enable": true,
          "speed": 3000,
          "opacity_min": 0.1,
          "sync": false
        }
      },
      "size": {
        "value": 2.8,
        "value_min": 1,
        "random": true
      },
      "line_linked": {
        "enable": false
      },
      "move": {
        "enable": true,
        "speed": 550,  // in pixel-per-second
        "direction": "bottom",
        "random": false,
        "straight": true,
        "out_mode": "out",
        "attract": {
          "enable": true,
          "rotateX": 600,
          "rotateY": 1200
        }
      },
      "trail": {
        "enable": true,
        "samples": 7,
        "discont_threshold": 4
      }
    },
    "interactivity": {
      "detect_on": "canvas",
      "events": {
        "onhover": {
          "enable": true,
          "mode": "repulse"
        },
        "resize": true
      },
      "modes": {
        "repulse": {
          "distance": 150
        }
      }
    },
    "retina_detect": true
  }

);
